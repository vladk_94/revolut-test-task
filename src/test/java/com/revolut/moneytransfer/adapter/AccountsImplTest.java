package com.revolut.moneytransfer.adapter;

import com.revolut.moneytransfer.domain.Account;
import com.revolut.moneytransfer.domain.Accounts;
import org.junit.Test;

import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountsImplTest {
    private Accounts accounts = new AccountsImpl();

    @Test
    public void savesAccount() {
        // when
        Account account = accounts.save(new Account("accountInfo"));
        // then
        assertThat(accounts.findOne(account.getId())).isEqualTo(account);
    }

    @Test
    public void findsAll() {
        // given
        Account accountOne = accounts.save(new Account("accountInfo"));
        Account accountTwo = accounts.save(new Account("accountInfo2"));
        // when
        Collection<Account> accounts = this.accounts.findAll();
        // then
        assertThat(accounts).containsExactlyInAnyOrder(accountOne, accountTwo);
    }
}