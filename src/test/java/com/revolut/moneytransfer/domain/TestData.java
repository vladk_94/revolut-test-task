package com.revolut.moneytransfer.domain;

import java.math.BigDecimal;

import static java.time.LocalDate.of;

public class TestData {
    public static Account anEmptyAccount(long id) {
        Account account = new Account("accountInfo");
        account.setId(id);
        return account;
    }

    public static Account anAccount(long id, BigDecimal initialAmount) {
        Account account = anEmptyAccount(id);
        account.addEntry(new Entry(account, initialAmount, null, of(2019, 1, 1)));
        return account;
    }
}
