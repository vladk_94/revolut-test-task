package com.revolut.moneytransfer.domain;

import org.junit.Test;

import java.math.BigDecimal;

import static com.revolut.moneytransfer.domain.TestData.anEmptyAccount;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static java.time.LocalDate.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

public class TransactionTest {

    @Test
    public void addsEntry() {
        // given
        Transaction transaction = Transaction.begin(of(2018, 1, 1));
        // when
        transaction.add(valueOf(500), anEmptyAccount(1));
        // then
        assertThat(transaction.getEntries())
                .hasSize(1)
                .contains(new Entry(anEmptyAccount(1), valueOf(500),
                        transaction, of(2018, 1, 1)));
    }

    @Test
    public void commitsTransaction() {
        // given
        Account accountOne = anEmptyAccount(1);
        Account accountTwo = anEmptyAccount(2);
        Transaction transaction = Transaction.begin(of(2018, 1, 1));
        transaction.add(valueOf(500), accountOne);
        transaction.add(valueOf(700), accountTwo);
        // when
        transaction.commit();
        // then
        assertThat(accountOne.getBalance()).isCloseTo(valueOf(500), within(valueOf(1e-6)));
        assertThat(accountTwo.getBalance()).isCloseTo(valueOf(700), within(valueOf(1e-6)));
    }

    @Test
    public void rollbacksTransaction() {
        // given
        Account accountOne = anEmptyAccount(1);
        Account accountTwo = anEmptyAccount(2);
        Transaction transaction = prepareTransaction(accountOne, accountTwo);
        // when
        transaction.rollback();
        // then
        assertThat(accountOne.getBalance()).isCloseTo(ZERO, within(valueOf(1e-6)));
        assertThat(accountTwo.getBalance()).isCloseTo(ZERO, within(valueOf(1e-6)));
    }

    private Transaction prepareTransaction(Account accountOne, Account accountTwo) {
        Transaction transaction = Transaction.begin(of(2018, 1, 1));
        transaction.add(valueOf(500), accountOne);
        transaction.add(valueOf(700), accountTwo);
        transaction.commit();
        return transaction;
    }
}