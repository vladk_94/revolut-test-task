package com.revolut.moneytransfer.domain;

import org.junit.Test;

import java.math.BigDecimal;

import static com.revolut.moneytransfer.domain.TestData.anAccount;
import static com.revolut.moneytransfer.domain.TestData.anEmptyAccount;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static java.time.LocalDate.of;
import static org.assertj.core.api.Assertions.*;

public class AccountTest {
    @Test
    public void getsBalance() {
        // given
        Account account = anAccount(1, valueOf(600));
        // when
        BigDecimal balance = account.getBalance();
        // then
        assertThat(balance).isCloseTo(valueOf(600), within(valueOf(1e-10)));
    }

    @Test
    public void getsBalance_returnsZeroIfNoEntriesWritten() {
        // given
        Account account = anEmptyAccount(1);
        // when
        BigDecimal balance = account.getBalance();
        // then
        assertThat(balance).isCloseTo(ZERO, within(valueOf(1e-10)));
    }

    @Test
    public void withdrawsMoney_throwsExceptionWhenNotEnoughMoneyForTransaction() {
        // given
        Account account = anEmptyAccount(1);
        // when
        Throwable throwable = catchThrowable(() -> account.withdraw(valueOf(300), anEmptyAccount(2), of(2018, 1, 1)));
        // then
        assertThat(throwable).isExactlyInstanceOf(IllegalStateException.class).hasMessageContaining("not enough money");
    }

    @Test
    public void withdrawsMoney_throwsExceptionWhenTargetIsTheSameAsSource() {
        // given
        Account account = anAccount(1, valueOf(600));
        // when
        Throwable throwable = catchThrowable(() -> account.withdraw(valueOf(300), account, of(2018, 1, 1)));
        // then
        assertThat(throwable).isExactlyInstanceOf(IllegalArgumentException.class).hasMessageContaining("Can't withdraw money to itself");
    }

    @Test
    public void withdrawsMoney() {
        // given
        Account account = anAccount(1, valueOf(600));
        // when
        account.withdraw(valueOf(300), anEmptyAccount(2), of(2018, 1, 1));
        // then
        assertThat(account.getBalance()).isCloseTo(valueOf(300), within(valueOf(1e-10)));
    }

    @Test
    public void getsTransactions() {
        // given
        Account account = anAccount(1, valueOf(600));
        // when
        account.withdraw(valueOf(100), anEmptyAccount(2), of(2017, 12, 1));
        account.withdraw(valueOf(300), anEmptyAccount(2), of(2018, 2, 1));
        // then
        assertThat(account.getTransactionsUpTo(of(2018, 1, 1)))
                .extracting(Transaction::getDate)
                .containsExactlyInAnyOrder(of(2017, 12, 1));
    }

    @Test
    public void depositsMoneyToAccount() {
        // given
        Account account = anEmptyAccount(1);
        // when
        account.deposit(valueOf(300), of(2018, 1, 1));
        // then
        assertThat(account.getBalance()).isCloseTo(valueOf(300), within(valueOf(1e-10)));
    }
}