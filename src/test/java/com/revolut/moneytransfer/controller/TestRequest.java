package com.revolut.moneytransfer.controller;

import spark.Request;

import java.util.HashMap;
import java.util.Map;

public class TestRequest extends Request {
    private final Map<String, String> queryMap = new HashMap<>();

    protected TestRequest() {
        super();
    }

    @Override
    public String queryParams(String queryParam) {
        return queryMap.get(queryParam);
    }

    public void addParam(String key, String value) {
        queryMap.put(key, value);
    }
}
