package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.domain.Transaction;
import com.revolut.moneytransfer.service.AccountsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.List;

import static com.revolut.moneytransfer.domain.TestData.anEmptyAccount;
import static java.math.BigDecimal.valueOf;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountControllerTest {
    @Mock
    private AccountsService accountsService;
    @InjectMocks
    private AccountController controller;

    @Test
    public void createsAccount() {
        // given
        TestRequest testRequest = new TestRequest();
        testRequest.addParam("accountInfo", "accountInfo");
        // when
        controller.createAccount(testRequest);
        // then
        verify(accountsService).createAccount("accountInfo");
    }

    @Test
    public void receivesAccountBalance() {
        // given
        TestRequest testRequest = new TestRequest();
        testRequest.addParam("id", "1");
        // when
        controller.getAccountBalance(testRequest);
        // then
        verify(accountsService).getAccountBalance(1L);
    }

    @Test
    public void depositsMoney() {
        // given
        TestRequest testRequest = new TestRequest();
        testRequest.addParam("id", "1");
        testRequest.addParam("amount", "200");
        // when
        controller.deposit(testRequest);
        // then
        verify(accountsService).deposit(1L, valueOf(200));
    }

    @Test
    public void withdrawsMoney() {
        // given
        TestRequest testRequest = new TestRequest();
        testRequest.addParam("originatorId", "1");
        testRequest.addParam("beneficiaryId", "2");
        testRequest.addParam("amount", "200");
        // when
        controller.withdraw(testRequest);
        // then
        verify(accountsService).withdraw(1L, 2L, valueOf(200));
    }

    @Test
    public void getAllTransactionsByDate() {
        // given
        TestRequest testRequest = new TestRequest();
        testRequest.addParam("id", "1");
        testRequest.addParam("date", "2018-10-01");
        when(accountsService.getTransactionsUpToDate(1L, LocalDate.of(2018, 10, 1)))
                .thenReturn(transactions());
        // when
        List<TransactionDto> transactions = controller.getAllTransactionsByDate(testRequest);
        // then
        assertThat(transactions)
                .extracting(TransactionDto::getDate)
                .containsOnly(LocalDate.of(2018, 10, 1), LocalDate.of(2018, 11, 1));
    }

    private List<Transaction> transactions() {
        Transaction transaction = Transaction.begin(LocalDate.of(2018, 10, 1));
        transaction.add(valueOf(200), anEmptyAccount(1));

        Transaction transactionTwo = Transaction.begin(LocalDate.of(2018, 11, 1));
        transaction.add(valueOf(400), anEmptyAccount(2));
        return asList(transaction, transactionTwo);
    }
}