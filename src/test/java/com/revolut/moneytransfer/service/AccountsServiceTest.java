package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.domain.Account;
import com.revolut.moneytransfer.domain.Accounts;
import com.revolut.moneytransfer.domain.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.revolut.moneytransfer.domain.TestData.anAccount;
import static com.revolut.moneytransfer.domain.TestData.anEmptyAccount;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountsServiceTest {
    @Mock
    private Clock clock;
    @Mock
    private Accounts accounts;
    @InjectMocks
    private AccountsServiceImpl accountsService;

    @Test
    public void createsAccount() {
        // given
        String accountInfo = "accountInfo";
        when(accounts.save(new Account(accountInfo))).thenReturn(anEmptyAccount(1));
        // when
        Account account = accountsService.createAccount(accountInfo);
        // then
        assertThat(account.getAccountInfo()).isEqualTo(accountInfo);
    }

    @Test
    public void receivesAccountBalance() {
        // given
        Long accountId = 1L;
        when(accounts.findOne(accountId)).thenReturn(anAccount(1, valueOf(300)));
        // when
        BigDecimal accountBalance = accountsService.getAccountBalance(accountId);
        // then
        assertThat(accountBalance).isCloseTo(valueOf(300), within(valueOf(1e-8)));
    }

    @Test
    public void receivesAccountBalance_throwsExceptionWhenAccountWithRequestedIdNotFound() {
        // given
        Long accountId = 1L;
        when(accounts.findOne(accountId)).thenReturn(null);
        // when
        Throwable throwable = catchThrowable(() -> accountsService.getAccountBalance(accountId));
        // then
        assertThat(throwable).isExactlyInstanceOf(IllegalArgumentException.class).hasMessageContaining("not found");
    }

    @Test
    public void depositsMoney_throwsExceptionWhenAccountWithRequestedIdNotFound() {
        // given
        Long accountId = 1L;
        when(accounts.findOne(accountId)).thenReturn(null);
        // when
        Throwable throwable = catchThrowable(() -> accountsService.deposit(accountId, valueOf(600)));
        // then
        assertThat(throwable).isExactlyInstanceOf(IllegalArgumentException.class).hasMessageContaining("not found");
    }

    @Test
    public void depositsMoney() {
        // given
        long accountId = 1L;
        when(accounts.findOne(accountId)).thenReturn(anAccount(accountId, valueOf(300)));
        when(clock.instant()).thenReturn(Instant.parse("2018-12-01T00:00:00.00Z"));
        when(clock.getZone()).thenReturn(ZoneId.systemDefault());
        // when
        accountsService.deposit(accountId, valueOf(600));
        // then
        assertThat(accountsService.getAccountBalance(accountId))
                .isCloseTo(valueOf(900), within(valueOf(1e-8)));
    }

    @Test
    public void withdrawsMoney_throwsExceptionWhenOriginatorAccountOrBeneficiaryAccountNotFound() {
        // given
        long originatorAccountId = 1L;
        long beneficiaryAccountId = 2L;
        when(accounts.findOne(originatorAccountId)).thenReturn(null);
        when(accounts.findOne(beneficiaryAccountId)).thenReturn(anAccount(beneficiaryAccountId, valueOf(600)));
        // when
        Throwable throwable = catchThrowable(() -> accountsService.withdraw(originatorAccountId, beneficiaryAccountId, valueOf(600)));
        // then
        assertThat(throwable).isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining(originatorAccountId + " not found");
    }

    @Test
    public void withdrawsMoney() {
        // given
        long originatorAccountId = 1L;
        long beneficiaryAccountId = 2L;
        prepareTestData(originatorAccountId, beneficiaryAccountId);
        // when
        accountsService.withdraw(originatorAccountId, beneficiaryAccountId, valueOf(300));
        // then
        assertThat(accountsService.getAccountBalance(originatorAccountId)).isCloseTo(ZERO, within(valueOf(1e-8)));
        assertThat(accountsService.getAccountBalance(beneficiaryAccountId)).isCloseTo(valueOf(900), within(valueOf(1e-8)));
    }

    @Test
    public void receivesAccountTransactions() {
        // given
        long accountId = accountWithThreeTransactions();
        LocalDate date = LocalDate.of(2018, 12, 1);
        // when
        List<Transaction> transactions = accountsService.getTransactionsUpToDate(accountId, date);
        // then
        assertThat(transactions).extracting(Transaction::getDate).containsOnly(date);
    }

    private long accountWithThreeTransactions() {
        long accountId = 1;
        Account account = anEmptyAccount(accountId);
        account.deposit(valueOf(200), LocalDate.of(2018, 12, 1));
        account.deposit(valueOf(400), LocalDate.of(2018, 12, 1));
        account.withdraw(valueOf(400), anEmptyAccount(2), LocalDate.of(2018, 12, 1));
        when(accounts.findOne(accountId)).thenReturn(account);
        return accountId;
    }

    private void prepareTestData(long originatorAccountId, long beneficiaryAccountId) {
        when(accounts.findOne(originatorAccountId)).thenReturn(anAccount(originatorAccountId, valueOf(300)));
        when(accounts.findOne(beneficiaryAccountId)).thenReturn(anAccount(beneficiaryAccountId, valueOf(600)));
        when(clock.instant()).thenReturn(Instant.parse("2018-12-01T00:00:00.00Z"));
        when(clock.getZone()).thenReturn(ZoneId.systemDefault());
    }
}