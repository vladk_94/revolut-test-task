package com.revolut.moneytransfer;

import com.revolut.moneytransfer.controller.AccountController;
import lombok.RequiredArgsConstructor;
import spark.servlet.SparkApplication;

import javax.inject.Inject;

import static com.revolut.moneytransfer.JsonTransformer.newTransformer;
import static spark.Spark.*;

@RequiredArgsConstructor(onConstructor = @__({@Inject}))
public class SparkApplicationImpl implements SparkApplication {
    private static final String APPLICATION_JSON = "application/json";
    private final AccountController accountController;

    @Override
    public void init() {
        post("/accounts/withdraw", APPLICATION_JSON, (request, response) -> accountController.withdraw(request));
        post("/accounts/deposit", APPLICATION_JSON, (request, response) -> accountController.deposit(request));
        get("/accounts/balance", APPLICATION_JSON, (request, response) -> accountController.getAccountBalance(request), newTransformer());
        get("/accounts/transactions", APPLICATION_JSON, (request, response) -> accountController.getAllTransactionsByDate(request), newTransformer());
        put("/accounts/create", APPLICATION_JSON, (request, response) -> accountController.createAccount(request), newTransformer());
    }
}
