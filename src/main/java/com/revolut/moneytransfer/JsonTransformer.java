package com.revolut.moneytransfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.RequiredArgsConstructor;
import spark.ResponseTransformer;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor(access = PRIVATE)
public class JsonTransformer implements ResponseTransformer {
    private final ObjectMapper objectMapper;

    public static ResponseTransformer newTransformer(ObjectMapper customMapper) {
        return new JsonTransformer(customMapper);
    }

    public static ResponseTransformer newTransformer() {
        ObjectMapper objectMapper = new ObjectMapper()
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                .registerModule(new JavaTimeModule());
        return newTransformer(objectMapper);
    }

    @Override
    public String render(Object model) throws Exception {
        return objectMapper.writeValueAsString(model);
    }
}