package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.domain.Account;
import com.revolut.moneytransfer.domain.Accounts;
import com.revolut.moneytransfer.domain.Transaction;
import lombok.RequiredArgsConstructor;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor(onConstructor = @__({@Inject}))
public class AccountsServiceImpl implements AccountsService {
    private final Accounts accounts;
    private final Clock clock;

    @Override
    public Account createAccount(String accountInfo) {
        return accounts.save(new Account(accountInfo));
    }

    @Override
    public BigDecimal getAccountBalance(Long accountId) {
        Account account = accounts.findOne(accountId);
        validateAccountExists(account, accountId);
        return account.getBalance();
    }

    @Override
    public void deposit(Long id, BigDecimal amount) {
        Account account = accounts.findOne(id);
        validateAccountExists(account, id);
        account.deposit(amount, LocalDate.now(clock));
    }

    @Override
    public void withdraw(Long originId, Long beneficiaryId, BigDecimal payment) {
        Account originator = accounts.findOne(originId);
        Account beneficiary = accounts.findOne(beneficiaryId);
        validateAccountExists(originator, originId);
        validateAccountExists(beneficiary, beneficiaryId);
        originator.withdraw(payment, beneficiary, LocalDate.now(clock));
    }

    @Override
    public List<Transaction> getTransactionsUpToDate(Long accountId, LocalDate date) {
        Account account = accounts.findOne(accountId);
        validateAccountExists(account, accountId);
        return account.getTransactionsUpTo(date);
    }

    private void validateAccountExists(Account account, Long id) {
        if (account == null) {
            throw new IllegalArgumentException("Requested account with id " +  id  + " not found");
        }
    }
}
