package com.revolut.moneytransfer.service;

import com.revolut.moneytransfer.domain.Account;
import com.revolut.moneytransfer.domain.Transaction;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public interface AccountsService {
    Account createAccount(String accountInfo);

    BigDecimal getAccountBalance(Long accountId);

    void deposit(Long id, BigDecimal amount);

    void withdraw(Long originId, Long beneficiaryId, BigDecimal payment);

    List<Transaction> getTransactionsUpToDate(Long id, LocalDate date);
}
