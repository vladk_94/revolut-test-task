package com.revolut.moneytransfer.domain;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.math.BigDecimal.ZERO;
import static java.util.stream.Collectors.toList;


@RequiredArgsConstructor
@EqualsAndHashCode(of = "id")
@Getter
public class Account {
    private final String accountInfo;
    @Getter(value = AccessLevel.PACKAGE)
    private final Collection<Entry> entries = ConcurrentHashMap.newKeySet();
    @Getter(value = AccessLevel.PACKAGE)
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    @Setter
    private Long id;

    public BigDecimal getBalance() {
        lock.readLock().lock();
        try {
            return entries.stream().map(Entry::getAmount).reduce(ZERO, BigDecimal::add);
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<Transaction> getTransactionsUpTo(LocalDate date) {
        lock.readLock().lock();
        try {
            return entries.stream()
                    .filter(e -> e.getDate().isBefore(date) || e.getDate().isEqual(date))
                    .map(Entry::getTransaction)
                    .collect(toList());
        } finally {
            lock.readLock().unlock();
        }
    }

    public void withdraw(BigDecimal amount, Account target, LocalDate date) {
        Transaction transaction = Transaction.begin(date);
        try {
            validate(amount, target);
            transaction.add(amount.negate(), this);
            transaction.add(amount, target);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    public void deposit(BigDecimal amount, LocalDate date) {
        Transaction transaction = Transaction.begin(date);
        try {
            transaction.add(amount, this);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    void addEntry(Entry entry) {
        entries.add(entry);
    }

    void removeEntry(Entry entry) {
        entries.remove(entry);
    }

    private void validate(BigDecimal amount, Account target) {
        if (!hasEnoughMoneyForPayment(amount)) {
            throw new IllegalStateException("Can't perform operation: not enough money");
        }
        if (target.equals(this)) {
            throw new IllegalArgumentException("Can't withdraw money to itself");
        }
    }

    private boolean hasEnoughMoneyForPayment(BigDecimal payment) {
        return getBalance().compareTo(payment) >= 0;
    }
}
