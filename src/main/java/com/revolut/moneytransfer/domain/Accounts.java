package com.revolut.moneytransfer.domain;

import java.util.Collection;

public interface Accounts {
    Account findOne(Long id);

    Collection<Account> findAll();

    Account save(Account account);
}
