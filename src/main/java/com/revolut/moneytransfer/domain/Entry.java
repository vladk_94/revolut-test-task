package com.revolut.moneytransfer.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
public class Entry {
    private final Account account;
    private final BigDecimal amount;
    private final Transaction transaction;
    private final LocalDate date;

    public void commit() {
        account.addEntry(this);
    }

    public void rollback() {
        account.removeEntry(this);
    }
}
