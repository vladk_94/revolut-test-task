package com.revolut.moneytransfer.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;

@RequiredArgsConstructor
@EqualsAndHashCode(of = {"id"})
@Getter
public class Transaction {
    private static final AtomicLong counter = new AtomicLong();

    private final Collection<Entry> entries = new HashSet<>();
    private final Long id;
    private final LocalDate date;

    public static Transaction begin(LocalDate date) {
        return new Transaction(counter.incrementAndGet(), date);
    }

    public void add(BigDecimal amount, Account target) {
        entries.add(new Entry(target, amount, this, date));
    }

    public void commit() {
        lock();
        try {
            entries.forEach(Entry::commit);
        } finally {
            unlock();
        }
    }

    public void rollback() {
        lock();
        try {
            entries.forEach(Entry::rollback);
        } finally {
            unlock();
        }
    }

    private void lock() {
        getAccountsWriteLocks().forEach(Lock::lock);
    }

    private void unlock() {
        getAccountsWriteLocks().forEach(Lock::unlock);
    }

    private Stream<Lock> getAccountsWriteLocks() {
        return entries.stream()
                .map(Entry::getAccount)
                .sorted(comparing(Account::getId))
                .map(Account::getLock)
                .map(ReadWriteLock::writeLock);
    }
}
