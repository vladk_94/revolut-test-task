package com.revolut.moneytransfer.adapter;

import com.revolut.moneytransfer.domain.Account;
import com.revolut.moneytransfer.domain.Accounts;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class AccountsImpl implements Accounts {
    private final Map<Long, Account> accounts = new ConcurrentHashMap<>();
    private final AtomicLong counter = new AtomicLong();

    @Override
    public Account findOne(Long id) {
        return accounts.get(id);
    }

    @Override
    public Collection<Account> findAll() {
        return accounts.values();
    }

    @Override
    public Account save(Account account) {
        if (account.getId() == null) {
            account.setId(counter.incrementAndGet());
        }
        accounts.put(account.getId(), account);
        return account;
    }
}
