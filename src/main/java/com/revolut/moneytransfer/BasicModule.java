package com.revolut.moneytransfer;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.revolut.moneytransfer.adapter.AccountsImpl;
import com.revolut.moneytransfer.controller.AccountController;
import com.revolut.moneytransfer.domain.Accounts;
import com.revolut.moneytransfer.service.AccountsService;
import com.revolut.moneytransfer.service.AccountsServiceImpl;
import spark.servlet.SparkApplication;

import java.time.Clock;

public class BasicModule extends AbstractModule {

    @SuppressWarnings("UninstantiableBinding")
    @Override
    protected void configure() {
        bind(SparkApplication.class).to(SparkApplicationImpl.class);
    }

    @Provides
    @Singleton
    public Accounts accounts() {
        return new AccountsImpl();
    }

    @Provides
    @Singleton
    public AccountController accountController(AccountsService accountsService) {
        return new AccountController(accountsService);
    }

    @Provides
    @Singleton
    public AccountsService accountsService(Accounts accounts, Clock clock) {
        return new AccountsServiceImpl(accounts, clock);
    }

    @Provides
    @Singleton
    public Clock clock() {
        return Clock.systemDefaultZone();
    }
}
