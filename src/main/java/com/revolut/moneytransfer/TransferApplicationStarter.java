package com.revolut.moneytransfer;

import com.google.inject.Injector;
import spark.servlet.SparkApplication;

import static com.google.inject.Guice.createInjector;

public class TransferApplicationStarter {

    public static void main(String[] args) {
        Injector injector = createInjector(new BasicModule());
        injector.getInstance(SparkApplication.class).init();
    }
}
