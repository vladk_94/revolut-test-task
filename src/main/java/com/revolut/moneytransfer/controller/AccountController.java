package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.domain.Account;
import com.revolut.moneytransfer.service.AccountsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import spark.Request;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.lang.Long.parseLong;
import static java.util.stream.Collectors.toList;

@RequiredArgsConstructor(onConstructor = @__({@Inject}))
@Slf4j
public class AccountController {
    private static final String SUCCESS = "Success";
    private final AccountsService accountsService;

    public Account createAccount(Request request) {
        log.info("Got request {}", request.queryParams("accountInfo"));
        return accountsService.createAccount(request.queryParams("accountInfo"));
    }

    public BigDecimal getAccountBalance(Request request) {
        String id = checkNotNull(request.queryParams("id"));
        log.info("Got account balance request {}", id);
        BigDecimal accountBalance = accountsService.getAccountBalance(parseLong(id));
        log.info("Got response balance: {}", accountBalance);
        return accountBalance;
    }

    public String deposit(Request request) {
        String id = checkNotNull(request.queryParams("id"));
        String amount = checkNotNull(request.queryParams("amount"));
        log.info("Got deposit request for account {} with amount {}", id, amount);
        accountsService.deposit(parseLong(id), new BigDecimal(amount));
        return SUCCESS;
    }

    public String withdraw(Request request) {
        String originatorId = checkNotNull(request.queryParams("originatorId"));
        String beneficiaryId = checkNotNull(request.queryParams("beneficiaryId"));
        String amount = checkNotNull(request.queryParams("amount"));
        log.info("Got withdraw request from {} to {} with amount {}", originatorId, beneficiaryId, amount);
        accountsService.withdraw(parseLong(originatorId), parseLong(beneficiaryId), new BigDecimal(amount));
        return SUCCESS;
    }

    public List<TransactionDto> getAllTransactionsByDate(Request request) {
        String date = checkNotNull(request.queryParams("date"));
        String id = checkNotNull(request.queryParams("id"));
        log.info("Got transactions request for account {} up to {}", id, date);
        List<TransactionDto> transactions = accountsService.getTransactionsUpToDate(parseLong(id), LocalDate.parse(date)).stream()
                .map(TransactionDto::from)
                .collect(toList());
        log.info("Got transactions {}", transactions);
        return transactions;
    }
}
