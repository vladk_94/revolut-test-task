package com.revolut.moneytransfer.controller;

import com.revolut.moneytransfer.domain.Entry;
import com.revolut.moneytransfer.domain.Transaction;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;

import static lombok.AccessLevel.PRIVATE;

@AllArgsConstructor(access = PRIVATE)
@Getter
@ToString
class TransactionDto {
    private LocalDate date;
    private Collection<EntryDto> entries;

    static TransactionDto from(Transaction transaction) {
        return new TransactionDto(transaction.getDate(), transaction.getEntries().stream()
                .map(EntryDto::from)
                .collect(Collectors.toSet()));
    }

    @AllArgsConstructor(access = PRIVATE)
    @Getter
    @ToString
    private static class EntryDto {
        private Long accountId;
        private BigDecimal amount;

        private static EntryDto from(Entry entry) {
            return new EntryDto(entry.getAccount().getId(), entry.getAmount());
        }
    }
}
